const { SerialPort } = require('serialport');
const { ReadlineParser } = require('@serialport/parser-readline');

let isVerbose;


let findPortsWithAnFTDIDevice = ()=>{
    return new Promise((resolve,reject)=>{
        SerialPort.list()
            .then((ports)=>{
                let devices = [];
                for(let i=0;i<ports.length;i++){
                    try{
                        if(ports[i].manufacturer === "FTDI"){
                            console.log(`Found what could be an Esper serial device on port ${ports[i].path}`);
                            devices.push(ports[i].path);
                        }
                    }catch(e){
                        console.log("ports returned did not have property 'manufacturer'");
                    }

                }
                resolve(devices);
            }).catch(reject);
    });
};

let pollFTDIDevicesToSeeIfTheyAreATriggerBox = (portPaths)=>{
    let promiseHolder = [];
    let triggerBoxes = [];
    for(let i=0;i<portPaths.length;i++){
        promiseHolder.push(new Promise((resolve,reject)=>{
            (()=>{
                let tb = new TriggerBox(portPaths[i],isVerbose);
                try {
                    tb.check().then((isBox) => {
                        if (isBox) {
                            triggerBoxes.push(tb);
                            console.log("Is a triggerbox");
                        }
                        resolve();
                    }).catch(reject);
                }catch(e){
                    console.log("error trying to construct tb - "+e);
                }
            })()

        }))
    }
    return Promise.all(promiseHolder).then(()=>{
        return Promise.resolve(triggerBoxes);
    })
};

class TriggerBox{
    constructor(port,verbose){
        this.verbose=verbose;
        try{
            this.serialPort = new SerialPort({ path:port, baudRate:115200 },(err) =>{
                if (err) {
                    return console.log('Error: ', err.message)
                    this.ok=false;
                }else{

                    this.parser = new ReadlineParser();
                    this.serialPort.pipe(this.parser);
                    this.parser.on('data', this.handleMessage);
                }
            });

        }catch(e){
            console.log(`error in triggerbox constructor ${e}`);
        }

    }
    stack=[];
    currentCommand = null;
    commandTimedOut(){
        console.log("command timed out");
        if(this.currentCommand.retries < 3){
            this.currentCommand.retries+=1;
            this.sendCurrentCommand();
        }else{
            console.log(`max number of retries - running error callback for command "${this.currentCommand.command}"`);
            this.currentCommand.onFail();
            this.nextCommand();
        }
    }

    handleMessage(msg){
        if(this.currentCommand === null){
            if(this.startedUp){
                console.log("unexpected message: -"+msg );
            }
        }else{
            clearTimeout(this.timeoutHolder);
            let status = this.currentCommand.responseAssessor(msg);
            if(status === 0){ //no problem
                this.currentCommand.onSuccess();
                this.nextCommand();
            }else{
                if(status < 1){
                    console.log("response assessor rejected");
                    this.currentCommand.onFail();
                    this.nextCommand();
                }
            }
        }
    }

    defaultResponseAssessor = (msg)=>{
        if(msg === "tenFour"){
            return 0;
        }else if(msg ==="fourTen"){
            return 1;
        }else{
            console.log(`unexpected response - ${msg}`);
            return 1;
        }
    }
    check(){

        if(this.ok){
            return new Promise((resolve)=>{
                this.sendCommand("*calling_ESPER_triggerBox",
                    ()=>{resolve(true)},
                    ()=>{this.shutdown();resolve(false);},
                    (msg)=>{
                        if(msg.includes("TriggerBox:[")){
                            console.log("found a triggerbox");
                            this.sendCommand('^');
                            this.startedUp=true;
                            return 0;
                        }else if(!msg.includes("tenFour") && this.verbose){
                            console.log(msg);
                        }else{
                            return 1;
                        }
                    },
                    1000);
            });
        }else{
            return Promise.resolve(false);
        }
    }
    sendCurrentCommand(){
        this.serialPort.write(`${this.currentCommand.command}\n`);
        this.timeoutHolder = setTimeout(this.commandTimedOut,this.currentCommand.timeout);
    }
    nextCommand(){
        if(this.stack.length > 0){
            this.currentCommand = this.stack.shift();
            this.sendCurrentCommand();
        }else{
            this.currentCommand = null;
        }
    }
    trySend(){
        if( this.currentCommand === null && this.stack.length > 0){
            this.currentCommand = this.stack.shift();
            this.sendCurrentCommand();
        }
    }
    sendCommand(command,onSuccess =()=>{}, onFail= ()=>{}, responseAssessor=this.defaultResponseAssessor, timeout=500){
        this.stack.push({command,responseAssessor,onSuccess, onFail, retries:0, timeout});
        this.trySend();
    }
    serialPort = null;
    parser = null;
    timeoutHolder = null;
    startedUp = false;
    verbose=false;
    ok=true;

    shutdown(){
        if(this.serialPort.isOpen){
            this.serialPort.flush();
            console.log("closing port");
            this.serialPort.close((err)=>{
                if(err){
                    console.log("closing error");
                    console.log(err);
                }else{
                    console.log("closed successfully");
                    delete this.serialPort;
                    delete this.parser;
                }
            });
        }

    }

    setOutputMode(one,two,three,four,five,six){
        return new Promise((resolve,reject)=>{
            let cmd ='/o';
            if(one.toLowerCase() === "5v0"){cmd+=2}else if( one.toLowerCase() === "3v3"){cmd+=1}else if(one.toLowerCase() === "cc"){cmd+=0} else {console.log("first output mode not recognised, defaulting to 'cc'");cmd+=0;}
            if(two.toLowerCase() === "5v0"){cmd+=2}else if( two.toLowerCase() === "3v3"){cmd+=1}else if(two.toLowerCase() === "cc"){cmd+=0} else {console.log("second output mode not recognised, defaulting to 'cc'");cmd+=0;}
            if(three.toLowerCase() === "5v0"){cmd+=2}else if( three.toLowerCase() === "3v3"){cmd+=1}else if(three.toLowerCase() === "cc"){cmd+=0} else {console.log("third output mode not recognised, defaulting to 'cc'");cmd+=0;}
            if(four.toLowerCase() === "5v0"){cmd+=2}else if( four.toLowerCase() === "3v3"){cmd+=1}else if(four.toLowerCase() === "cc"){cmd+=0} else {console.log("fourth output mode not recognised, defaulting to 'cc'");cmd+=0;}
            if(five.toLowerCase() === "5v0"){cmd+=2}else if( five.toLowerCase() === "3v3"){cmd+=1}else if(five.toLowerCase() === "cc"){cmd+=0} else {console.log("fifth output mode not recognised, defaulting to 'cc'");cmd+=0;}
            if(six.toLowerCase() === "5v0"){cmd+=2}else if( six.toLowerCase() === "3v3"){cmd+=1}else if(six.toLowerCase() === "cc"){cmd+=0} else {console.log("sixth output mode not recognised, defaulting to 'cc'");cmd+=0;}

            this.sendCommand(cmd,resolve,reject);
            //this.serialPort.write(`/O\n`);
        })

    }

    saveSettings(){
        return new Promise((resolve,reject)=>{
            this.sendCommand('O',resolve,reject);
        });
    }
    setInputMode(mode){
        return new Promise((resolve,reject)=>{
            let cmd = '/i';
            let ok =true;
            switch(mode.toLowerCase()){
                case "5vttl":
                    cmd+="2";
                    break;
                case "cc":
                    cmd+="0";
                    break;
                case "rs485":
                    cmd+="3";
                    break;
                default:
                    ok = false;
                    console.log("input mode not recognised, possible : '5vttl', 'cc' or 'rs485'");
            }
            this.sendCommand(cmd,resolve,reject);
        })
    }

    setFocusEnable(one,two,three,four,five,six){
        return new Promise((resolve,reject)=>{
            let cmd ='f';
            if(one){cmd+=1}else {cmd+=0;}
            if(two){cmd+=1}else {cmd+=0;}
            if(three){cmd+=1}else {cmd+=0;}
            if(four){cmd+=1}else {cmd+=0;}
            if(five){cmd+=1}else {cmd+=0;}
            if(six){cmd+=1}else {cmd+=0;}

            this.sendCommand(cmd,resolve,reject);
        })
        //this.serialPort.write(`V\n`);
    }
    setShutterEnable(one,two,three,four,five,six){
        return new Promise((resolve,reject)=>{
            let cmd ='s';
            if(one){cmd+=1}else {cmd+=0;}
            if(two){cmd+=1}else {cmd+=0;}
            if(three){cmd+=1}else {cmd+=0;}
            if(four){cmd+=1}else {cmd+=0;}
            if(five){cmd+=1}else {cmd+=0;}
            if(six){cmd+=1}else {cmd+=0;}

            this.sendCommand(cmd,resolve,reject);
        })
        //this.serialPort.write(`G\n`);
    }

}





module.exports = {
    getTriggerBoxes : (verbose)=>{
        isVerbose = !!verbose;
        return findPortsWithAnFTDIDevice()
            .then(pollFTDIDevicesToSeeIfTheyAreATriggerBox)
            .then((triggerboxArray)=>{
                console.log(`found ${triggerboxArray.length} triggerboxes`);
                return Promise.resolve(triggerboxArray);
            })
            .catch((E)=>{console.log(`caught :${E}`);});
    }
}